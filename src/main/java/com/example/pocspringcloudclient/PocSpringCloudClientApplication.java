package com.example.pocspringcloudclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocSpringCloudClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocSpringCloudClientApplication.class, args);
    }

}
