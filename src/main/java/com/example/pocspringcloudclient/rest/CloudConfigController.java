package com.example.pocspringcloudclient.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@Slf4j
public class CloudConfigController {

    @Value("${properties.profil}")
    private String profil;

    /*
        Profiles dev and e2e don't have the property properties.database.url, so it will take the value from the default profile
     */
    @Value("${properties.database.url}")
    private String url;

    @Value("${properties.database.username}")
    private String username;

    @Value("${properties.database.password}")
    private String password;


    @GetMapping("/test")
    public ResponseEntity<String> test(){
        String str = String.format("For the profil = %s, you're database credentials are : url = %s, username = %s, password = %s", profil, url, username, password);
        if(profil == null || url == null ||  username == null ||  password == null){
            log.warn(str);
            return ResponseEntity.notFound().build();
        } else {
            log.info(str);
            return ResponseEntity.ok().body(str);
        }
    }


}
